package space.ixtomix.schwlemesser

import android.os.Bundle
import android.hardware.SensorManager
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.app.Activity
import android.content.Context
import android.hardware.Sensor
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : Activity(), SensorEventListener {

    // private var mSensorManager: SensorManager? = null
    private var mPressure: Sensor? = null
    private var mRelativeHumidity: Sensor? = null

    private var Computer: Computations = Computations()

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        //mPressure = mSensorManager!!.getDefaultSensor(Sensor.TYPE_PRESSURE)
    }

    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {

    }

    override fun onSensorChanged(event: SensorEvent) {

        //val millibars_of_pressure = event.values[0]

    }

    override fun onResume() {
        super.onResume()
        //mSensorManager!!.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL)
        //mSensorManager!!.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_NORMAL)
    }

    override fun onPause() {
        super.onPause()
        //mSensorManager!!.unregisterListener(this)

    }

    fun onCalcClick(view: View) {

        Computer.getFullSteamPressure(realTemperature = temp.text.toString().toDouble())

        Computer.getMaxSteamThickness(Computer.realTemperature)

        Computer.getRealSteamPressure(relativeHumidity = relHumi.text.toString().toDouble())

        Computer.getRealSteamThickness(Computer.realTemperature)

        Computer.getDewpoint(Computer.realSteamPressure)

        var Dewpoint = Computer.dewpoint.toString().subSequence(0, 5)
        textViewDEWPOINT.text = Dewpoint.toString() + " °C"

    }
}