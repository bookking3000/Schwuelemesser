package space.ixtomix.schwlemesser

import kotlin.math.exp
import kotlin.math.ln

class Computations {

    val M: Double = 6.112
    val E: Double = 17.62
    val T: Double = 243.12
    val RI: Double = 461.5
    val ABS_ZERO: Double = 273.15

    //Set by User or Device!
    var realTemperature = 0.0
    var realRelativeHumidity = 0.0

    var fullSteamPressure: Double = 0.0
    var realSteamPressure: Double = 0.0
    var maxSteamThickness: Double = 0.0
    var realSteamThickness: Double = 0.0
    var dewpoint: Double = 0.0


    //Maximaler Dampfdruck
    fun getFullSteamPressure(realTemperature: Double): Double {
        this.realTemperature = realTemperature
        fullSteamPressure = M * exp(((E * realTemperature) / (T + realTemperature)))
        return fullSteamPressure
    }

    //Tats. Dampdruck
    fun getRealSteamPressure(relativeHumidity: Double): Double {

        realSteamPressure = fullSteamPressure / 100 * relativeHumidity
        return realSteamPressure
    }

    // Do this only after getting full steam pressure
    // Maximale Dampfgehalt
    fun getMaxSteamThickness(realTemperature: Double): Double {
        maxSteamThickness = (fullSteamPressure / (RI * (ABS_ZERO + realTemperature))) * 100000
        return maxSteamThickness
    }

    // Tats. Dampfgehalt
    fun getRealSteamThickness(realTemperature: Double): Double {

        realSteamThickness = (realSteamPressure / (RI * (ABS_ZERO + realTemperature))) * 100000
        return realSteamThickness

    }

    //Taupunkt
    fun getDewpoint(realSteamPressure: Double): Double {
        dewpoint = 235 * ln(realSteamPressure / M) / ((E - 0.5) - ln(realSteamPressure / M))
        return dewpoint
    }

    //Abs. Feuchte NN
    fun getAbsoluteHumidity(relativeHumidity: Double): Double {
        realRelativeHumidity = relativeHumidity
        return 216.7 * (relativeHumidity / 100 * M * exp((E / (T + realTemperature))) / (ABS_ZERO + realTemperature))
    }
}